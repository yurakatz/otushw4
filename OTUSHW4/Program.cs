﻿using System;
using System.Net;
using System.Text.RegularExpressions;

namespace OTUSHW4
{ //   var m1 = Regex.Matches(stringurl, @"(http|ftp|https|www)(://|\.)[A-Za-z0-9-_\.]*(\.)[a-z]*",
    internal class Program
    {
        private static void Main(string[] args)
        {
            string inputUrl;
            if (args?.Length > 0)
            {
                inputUrl = args[0];
            }
            else
            {
                Console.WriteLine("Input URL:");
                inputUrl = Console.ReadLine();
            }


            try
            {
                Console.WriteLine("Downloading ...");
                var w = new WebClient();
                var siteString = w.DownloadString(inputUrl ?? throw new InvalidOperationException());
                var matches = Regex.Matches(siteString,
                    @"href\s*=\s*(?:[""'](?<1>[^""']*)[""']|(?<1>\S+))",
                    RegexOptions.Singleline);
                foreach (var i in matches) Console.WriteLine(i);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex}");
            }
        }
    }
}